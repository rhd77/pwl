<?php
class Fruit {
  public $name;
  public $color;
  public function __construct($name, $color) {
    $this->name = $name;
    $this->color = $color;
  }
  public function intro() {
    echo "Buahnya {$this->name} and the color is {$this->color}.";
  }
}

// Strawberry diwarisi dari Buah
class Strawberry extends Fruit {
  public function message() {
    echo "Apakah saya buah atau beri? ";
  }
}
$strawberry = new Strawberry("Strawberry", "merah");
$strawberry->message();
$strawberry->intro();
?>
<?php
        class Mobil {
          public function bunyi() {
            return "BRUMM BRUMM!";
          }
        }
         
        class Sepeda extends Mobil {
            public function bunyi() { // Implementasi Override
            return "Beeb Beeb!";
          }
        }
         
       $kendaraan1 = new Sepeda();
       
       echo $kendaraan1->bunyi();
?>
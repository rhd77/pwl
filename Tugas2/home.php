<?php
	session_start();
	if($_SESSION['isLogin'] != true || $_SESSION['jam_selesai']==date("Y-m-d H:i:s"))
	{
		header("Location: form_login.php?message=nologin");
	}
	echo "Selamat datang, ",strtoupper($_SESSION['uname'])," login pada: ",$_SESSION['jam_mulai'];
	echo "<br>";
?>
	<a href="logout.php">Logout</a>

    <h1>Form</h1><br>
    <form  method="post" action="simpandata.php">
        <p>Nama Lengkap:
        <input type="text" name="tnama" required>
        </p>

        <p>Alamat:<br>
        <textarea id="alamat" name="talamat" rows="2" cols="30"></textarea>
        </p>

        <p>Jenis kelamin :<br>
        <input type="radio" id="tlaki" name="tjeniskelamin" value="Laki laki">
        <label for="tlaki">Laki laki</label><br>
        <input type="radio" id="tperempuan" name="tjeniskelamin" value="Perempuan">
        <label for="tperempuan">Perempuan</label><br>

        <p>Hobi :<br>
        <input type="checkbox" name="hobi[]" value="Berenang">
        <label for="tberenang">Berenang</label><br>
        <input type="checkbox" name="hobi[]" value="Bermain Game">
        <label for="tgame"> Bermain game</label><br>
        <input type="checkbox" name="hobi[]" value="Masak">
        <label for="tmasak"> Memasak</label><br><br>

        <p>Pekerjaan :<br>
        <input type="radio" id="tsoftwareengineer" name="tpekerjaan" value="Software Engineer">
        <label for="tsoftwareengineer">Software Engineer</label><br>
        <input type="radio" id="tfrontend" name="tpekerjaan" value="Front End Developer">
        <label for="tfrontend">Front End Developer</label><br>
        <input type="radio" id="tuidesign" name="tpekerjaan" value="UI Designer">
        <label for="tuidesign">UI Designer</label><br><br>

        <input type="submit" value="Simpan"><br>
    </form>
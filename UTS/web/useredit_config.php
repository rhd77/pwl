<?php
    include "../config/database.php";

    $uname = $_POST['username'];
    $email = $_POST['email'];
    $peran = $_POST['peran'];
    $telp = $_POST['telp'];
    $passwd = $_POST['passwd'];
    $cpasswd = $_POST['cpasswd'];
    $id_dari_form = $_POST['id'];

    if($passwd==$cpasswd)
    {
    	$id = explode("|",base64_decode($id_dari_form));

        $psw = password_hash($passwd,PASSWORD_DEFAULT);

	    $upd = $db->prepare("UPDATE users SET username=?,passwd=?,email=?,peran=?,telp=? WHERE id=?");
	    $upd->execute([$uname,$passwd,$email,$peran,$telp,$id[1]]);
        header("Location: view_data.php?message=success");  	
    }
    else
    {
        header("Location: useredit_form.php?message=not-match");
    }
?>
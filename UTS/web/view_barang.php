<!DOCTYPE html>
<html>
	<head>
		<title>Registration Form</title>
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
		<meta name="viewport" content="width=device-width, initial-scale=1">
	</head>
	<body>

			<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="home.php">Home</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" href="logout.php">Logout</a>
        </li>

      </ul>
    </div>
  </div>
</nav>
<div class="row">
			<div class="col-md-4">
			</div>
			<div class="col-md-4">
<?php

	session_start();
	if($_SESSION['isLogin'] != true || $_SESSION['jam_selesai']==date("Y-m-d H:i:s"))
	{
		header("Location: login.php?message=nologin");
	}
	echo "<p>Selamat datang, ",strtoupper($_SESSION['uname'])," login pada: ",$_SESSION['jam_mulai'];
	echo "<br>";
?>
	<a href="input_barang.php">Input Barang</a> 
                
<?php

    include "../config/database.php";

    $rs = $db->query("SELECT * FROM barang");

    $rs->setFetchMode(PDO::FETCH_OBJ);
?>
    <table border=1 cellspacing=20 cellpadding=20>
        <tr>
            <th>Nama barang</th>
            <th>Harga</th>
            <th>Gambar</th>
			<th>Sisa stok</th>
        </tr>
<?php
    $i = 1;
    while($data = $rs->fetch())
    {
?>
    <tr>
        <td><?php echo $data->nama?></td>
        <td><?php echo $data->harga?></td>
		<td><?php echo $data->gambar?></td>
        <td><?php echo $data->jml_stok?></td>
        <td><a href="editbarang_form.php">Edit</a> | <a href="delete.php?id=<?php echo base64_encode(sha1(rand())."|".$data->id)?>">Hapus</a></td>
    </tr>
<?php
        $i++;
    }
?>
</body>
</html>
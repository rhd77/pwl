<!DOCTYPE html>
<html>
	<head>
		<title>Registration Form</title>
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
		<meta name="viewport" content="width=device-width, initial-scale=1">
	</head>
	<body>

			<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="home.php">Home</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" href="logout.php">Logout</a>
        </li>

      </ul>
    </div>
  </div>
</nav>
<div class="row">
			<div class="col-md-4">
			</div>
			<div class="col-md-4">

	<a href="edit.php">Input Data Baru</a> 
                
<?php

    include "../config/database.php";

	$sr = $db->query("SELECT * FROM users");

    // $rs->setFetchMode(PDO::FETCH_ASSOC);
	$sr->setFetchMode(PDO::FETCH_OBJ);
?>
<table border=1 cellspacing=20 cellpadding=20>
        <tr>
            <th>Kode User</th>
            <th>Nama</th>
            <th>Email</th>
			<th>Telepon</th>
			<th>Peran</th>
        </tr>
<?php
    $i = 1;
    while($data = $sr->fetch())
    {
?>
    <tr>
        <td><?php echo $data->id?></td>
        <td><?php echo $data->username?></td>
		<td><?php echo $data->email?></td>
        <td><?php echo $data->telp?></td>
		<td><?php echo $data->peran?></td>
        <td><a href="useredit_form.php?id=<?php echo base64_encode(sha1(rand())."|".$data->id)?>">Edit</a> | <a href="delete.php?id=<?php echo base64_encode(sha1(rand())."|".$data->id)?>">Hapus</a></td>
    </tr>
<?php
        $i++;
    }
?>
    
</body>
</html>